<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCSV;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public $today = null;

    public function __construct()
    {
        $this->today = new Carbon('March 5, 2018');
    }

    /*
     * Show the dashboard
     */
    public function home()
    {
        return view('dashboard');
    }

    /*
     * Process uploaded CSV
     */
    public function process(StoreCSV $request)
    {
        $validated = $request->validated();
        $path = $validated['csv_file']->getRealPath();
        $csv = array_map('str_getcsv', file($path));

        $parsed = $this->parseCSV($csv);

        return view('dashboard', ['data' => $parsed]);
    }

    /*
     * Parse CSV data and mark sent invitations
     */
    public function parseCSV($csv)
    {
        $response = [];
        $response['titles'] = array_shift($csv);
        $response['rows'] = [];
        $response['sent'] = [];

        $csv = collect($csv)->map(function ($item, $key) use ($response) {
            $data = collect($item)->mapWithKeys(function ($row, $index) use ($response) {
                return [$response['titles'][$index] => $row];
            });
            return $data;
        })->sortBy('trans_date')->all();

        foreach ($csv as $index => $data) {
            $data['sent'] = 'n/a';

            // Invite has contact method?
            if (empty($data->get('cust_phone'))
                && empty($data->get('cust_email'))) {
                $response['rows'][] = $data->all();
                continue;
            }
            
            // Invite is more than 7 days old?
            $date = new Carbon($data->get('trans_date'));
            $daysOld = $date->diffInDays($this->today);
            if ($daysOld > 7) {
                $response['rows'][] = $data->all();
                continue;
            }

            $data['sent'] = (!empty($data->get('cust_phone')))
                ? 'phone'
                : 'email';
            $sendTo = $data['cust_'.$data['sent']];
        
            // Has been invited already?
            if (in_array($sendTo, $response['sent'])) {
                $data['sent'] = 'n/a';
                $response['rows'][] = $data->all();
                continue;
            }

            $response['sent'][$data->get('cust_num')] = $sendTo;
            $response['rows'][] = $data->all();
        }

        return $response;
    }
}
