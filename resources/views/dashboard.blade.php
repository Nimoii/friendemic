<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Invite Data Parser</title>

        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body class="bg-light">
        <div class="container">
            <div class="py-5 text-center">
                <h2>Invite Data Parser</h2>
            </div>
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <h4 class="mb-3">Parse CSV Data</h4>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="post" action="/" enctype="multipart/form-data">
                        @csrf

                        <div class="mb-3 form-group">
                            <label for="csv_file">CSV File</label>
                            <input type="file" name="csv_file" class="form-control-file" id="csv_file" required>
                        </div>

                        <hr class="mb-4">
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Parse CSV</button>
                    </form>
                </div>
            </div>

            @if (!empty($data))
                <div class="py-5 text-center">
                    <h3>CSV Data</h3>
                </div>
                <div class="row">
                    <div class="col-md-12 mx-auto">
                        <table class="table">
                            <thead>
                                <tr>
                                    @foreach($data['titles'] as $title)
                                        <th>{{$title}}</th>
                                    @endforeach
                                    <th>invite_sent</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data['rows'] as $row)
                                    <tr>
                                        @foreach($row as $key => $column)
                                            <td>{{ ($key == 'sent') 
                                                ? ($column == 'phone') 
                                                    ? 'text'
                                                    : $column
                                                : $column
                                            }}</td>
                                    
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif

            <footer class="my-5 pt-5 text-muted text-center text-small">
                <p class="mb-1">© {{date('Y')}} <a href="http://subtlebot.net">Subtlebot</a></p>
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="/invite_data_parser_chart.pdf">Process Flowchart</a></li>
                    <li class="list-inline-item"><a href="https://bitbucket.org/Nimoii/friendemic">Bitbucket Repository</a></li>
                </ul>
            </footer>
        </div>

        <script src="/js/app.js"></script>
    </body>
</html>
